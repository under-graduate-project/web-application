package com.uob.book_scraper;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookScraperApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookScraperApplication.class, args);
	}

}
