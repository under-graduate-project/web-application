package com.uob.book_scraper.config;

import com.uob.book_scraper.service.UserDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private UserDetailService userDetailService;

    @Autowired
    private AuthHandler authHandler;



    @Override
    protected void configure(HttpSecurity httpSecurity) throws Exception {
        httpSecurity
                .authorizeRequests()
                .antMatchers("/web/view","/web/view_register","/reader/register","/resources/**").permitAll()
                .antMatchers("/web/bookshelf_view","/web/book_view/**","/web/book_reader/**","/book/find_by_search_criteria/**","/book/verify/**").hasAnyRole("ADMIN","READER")
                .antMatchers("/book/**","/reader/**","/scrap/**").hasRole("ADMIN").and()
                .formLogin()
                .loginPage("/login").permitAll()
                .successHandler(authHandler)
                .failureUrl("/login?error").and().csrf().disable();

    }

    @Override
    public void configure(AuthenticationManagerBuilder authentication) throws Exception {
        authentication.userDetailsService(userDetailService).passwordEncoder(passwordEncoder());
    }

    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}
