package com.uob.book_scraper.controller;

import com.uob.book_scraper.dto.BookDto;
import com.uob.book_scraper.service.BookService;
import com.uob.book_scraper.util.BookSearchCriteria;
import com.uob.book_scraper.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/book")
public class BookController {

    private static final String BOOK_VIEW = "/admin/book_view";
    private static final String STORE_VIEW = "/web/store";

    private Logger logger = LoggerFactory.getLogger(BookController.class);

    @Autowired
    private BookService bookService;

    @GetMapping("/view")
    public ModelAndView viewBooks() {
        ModelAndView modelAndView = new ModelAndView(BOOK_VIEW);
        try {

            List<BookDto> bookDtoList = bookService.viewBooks();
            modelAndView.addObject("bookList", bookDtoList);
            modelAndView.addObject("bookDto", new BookDto());
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @PostMapping("/add")
    public ModelAndView saveNewBook(@ModelAttribute BookDto bookDto, ModelAndView modelAndView) {
        modelAndView.setViewName(BOOK_VIEW);
        try {
            boolean result = bookService.saveBook(bookDto);

            if (result) {
                modelAndView.addObject(Constant.SUCCESS, true);
                modelAndView.addObject(Constant.MESSAGE, "Book saved successfully.");
            } else {
                modelAndView.addObject(Constant.SUCCESS, false);
                modelAndView.addObject(Constant.MESSAGE, "Book save failure.");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            modelAndView.addObject(Constant.SUCCESS, false);
            modelAndView.addObject(Constant.MESSAGE, "Something went wrong.");
        }
        return modelAndView;
    }

    @GetMapping("/find")
    public ResponseEntity<BookDto> findBook(@RequestParam String id) {
        BookDto bookDto = new BookDto();
        try {
            bookDto = bookService.findById(id);

        } catch (Exception e) {
            e.printStackTrace();

        }
        return ResponseEntity.ok(bookDto);
    }

    @GetMapping("/find_by_search_criteria")
    public ModelAndView findBookBySearchCriteria(@RequestParam("searchCriteria") String searchCriteria, @RequestParam("searchString") String searchString) {

        ModelAndView modelAndView = new ModelAndView(STORE_VIEW);
        List<BookDto> bookDtoList = new ArrayList<>();
        try {
            bookDtoList = bookService.findBookBySearchCriteria(searchCriteria, searchString);

            modelAndView.addObject("bookList", bookDtoList);
            modelAndView.addObject("searchCriteria", BookSearchCriteria.values());
            modelAndView.addObject("searchString", searchString);
            modelAndView.addObject("searchedCriteria", searchCriteria);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @PostMapping("/update")
    public ModelAndView updateBook(@ModelAttribute BookDto bookDto, ModelAndView modelAndView) {
        modelAndView.setViewName(BOOK_VIEW);
        try {
            boolean result = bookService.updateBook(bookDto);

            if (result) {
                modelAndView.addObject(Constant.SUCCESS, true);
                modelAndView.addObject(Constant.MESSAGE, "Book updated successfully.");
            } else {
                modelAndView.addObject(Constant.SUCCESS, false);
                modelAndView.addObject(Constant.MESSAGE, "Book update failure.");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            modelAndView.addObject(Constant.SUCCESS, false);
            modelAndView.addObject(Constant.MESSAGE, "Something went wrong.");
        }
        return modelAndView;
    }

    @PostMapping("/verify/{id}")
    public ResponseEntity verifyFeedback(@PathVariable String id) {
        boolean result = false;
        try {
            result = bookService.verifyFeedback(id);

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return ResponseEntity.ok(result);
    }

}


