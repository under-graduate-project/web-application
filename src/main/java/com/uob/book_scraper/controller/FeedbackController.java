package com.uob.book_scraper.controller;

import com.uob.book_scraper.dto.FeedbackDto;
import com.uob.book_scraper.service.BookService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/feedback")
public class FeedbackController {

    @Autowired
    private BookService bookService;

    private Logger logger = LoggerFactory.getLogger(FeedbackController.class);

    @GetMapping("/view")
    public ModelAndView viewUserFeedbacks() {
        ModelAndView modelAndView = new ModelAndView("/admin/feedback_view");
        try {
            List<FeedbackDto> feedbackDtoList = bookService.viewUserFeedbacks();

            modelAndView.addObject("feedbackList", feedbackDtoList);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return modelAndView;
    }


}


