package com.uob.book_scraper.controller;

import com.uob.book_scraper.dto.ReaderDto;
import com.uob.book_scraper.service.ReaderService;
import com.uob.book_scraper.util.Constant;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/reader")
public class ReaderController {

    @Autowired
    private ReaderService readerService;

    private Logger logger = LoggerFactory.getLogger(ReaderController.class);

    @PostMapping("/register")
    public ModelAndView registerReader(@ModelAttribute ReaderDto readerDto) {
        ModelAndView modelAndView = new ModelAndView("/web/login");
        try {
            if (!readerDto.getPassword().equals(readerDto.getConfirmPassword())) {
                modelAndView = new ModelAndView("/web/register");
                modelAndView.addObject(Constant.SUCCESS, false);
                modelAndView.addObject(Constant.MESSAGE, "Confirm Password And Password should be the same.");

                modelAndView.addObject("signInObj", new ReaderDto());
                modelAndView.addObject("signUpObj", new ReaderDto());
                return modelAndView;
            }

            boolean result = readerService.registerReader(readerDto);
            if (result) {
                modelAndView.addObject(Constant.SUCCESS, true);
                modelAndView.addObject(Constant.MESSAGE, "Reader Registered Successfully.");
            } else {
                modelAndView.addObject(Constant.SUCCESS, false);
                modelAndView.addObject(Constant.MESSAGE, "Reader Registration Failed.");
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            modelAndView.addObject(Constant.SUCCESS, false);
            modelAndView.addObject(Constant.MESSAGE, "Something went wrong.");
        }
        modelAndView.addObject("signInObj", new ReaderDto());
        modelAndView.addObject("signUpObj", new ReaderDto());
        return modelAndView;
    }

    @PostMapping("/block_user/{id}")
    public ResponseEntity blockReader(@PathVariable String id) {
        boolean result = false;
        try {

            result = readerService.blockReader(Integer.parseInt(id));
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return ResponseEntity.ok(result);
    }

    @GetMapping("/view")
    public ModelAndView viewReaders() {
        ModelAndView modelAndView = new ModelAndView("/admin/reader_view");
        try {

            List<ReaderDto> readerDtoList = readerService.viewReaders();
            modelAndView.addObject("readersList", readerDtoList);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return modelAndView;
    }
}


