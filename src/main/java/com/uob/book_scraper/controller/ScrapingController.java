package com.uob.book_scraper.controller;

import com.uob.book_scraper.dto.ScrapingProcessDto;
import com.uob.book_scraper.dto.SuccessDto;
import com.uob.book_scraper.service.ScrapingProcessService;
import com.uob.book_scraper.util.ScrapingCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/scrap")
public class ScrapingController {

    @Autowired
    private ScrapingProcessService scrapingProcessService;

    private Logger logger = LoggerFactory.getLogger(ScrapingController.class);

    @Value("${app.web-scraper.url}")
    private String webScrapperUrl;

    @GetMapping("/view")
    public ModelAndView viewScrapingProsesses() {
        ModelAndView modelAndView = new ModelAndView("/admin/scraping_view");
        try {
            List<ScrapingProcessDto> scrapingProcessDtoList = scrapingProcessService.viewScrapingProcesses();
            boolean canEnable = scrapingProcessService.checkScrapingStatus();

            modelAndView.addObject("scrapingList", scrapingProcessDtoList);
            modelAndView.addObject("scrapingCriteria", ScrapingCriteria.values());
            modelAndView.addObject("canEnable", canEnable);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return modelAndView;
    }


    @GetMapping("/run_manual")
    public ResponseEntity<SuccessDto> runManualScrapingProcess(@RequestParam("category") String category) {
        SuccessDto successDto = new SuccessDto();
        try {
            ScrapingCriteria scrapingCriteria = ScrapingCriteria.valueOf(category);
            if (null != scrapingCriteria) {
                final String uri = webScrapperUrl.concat(scrapingCriteria.scrapingCriteria);
                RestTemplate restTemplate = new RestTemplate();
                String result = restTemplate.getForObject(uri, String.class);

                successDto.setMessage(result);
                successDto.setStatus(true);
            } else {
                successDto.setMessage("Invalid parameter!");
                successDto.setStatus(false);
            }

        } catch (Exception e) {
            successDto.setMessage("Scraping application error!");
            successDto.setStatus(false);
            logger.error(e.getMessage());
        }
        return ResponseEntity.ok(successDto);
    }
}


