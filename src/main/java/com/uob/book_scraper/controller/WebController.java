package com.uob.book_scraper.controller;

import com.uob.book_scraper.dto.BookDto;
import com.uob.book_scraper.dto.ReaderDto;
import com.uob.book_scraper.model.ReaderLog;
import com.uob.book_scraper.service.BookService;
import com.uob.book_scraper.util.BookSearchCriteria;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.List;

@RestController
@RequestMapping("/web")
public class WebController {

    private static final String FIRST_VIEW = "/web/first";
    private static final String STORE_VIEW = "/web/store";
    private static final String BOOK_VIEW = "/web/book_view";

    private Logger logger = LoggerFactory.getLogger(WebController.class);

    @Autowired
    private BookService bookService;

    @GetMapping("/view")
    public ModelAndView viewFirst() {
        ModelAndView modelAndView = new ModelAndView(FIRST_VIEW);
        try {
            modelAndView.addObject("signInObj", new ReaderDto());
            modelAndView.addObject("signUpObj", new ReaderDto());

        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping("/bookshelf_view")
    public ModelAndView viewBookShelf(Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView(STORE_VIEW);
        try {
            List<BookDto> bookDtoList = bookService.findActiveBooks("ACTIVE", authentication.getName());

            modelAndView.addObject("bookList", bookDtoList);
            modelAndView.addObject("searchCriteria", BookSearchCriteria.values());
        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping("/book_view/{id}")
    public ModelAndView viewBook(@PathVariable String id) {
        ModelAndView modelAndView = new ModelAndView(BOOK_VIEW);
        try {
            BookDto bookDto = bookService.findById(id);

            modelAndView.addObject("bookDto", bookDto);
        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping("/book_reader/{id}")
    public ModelAndView readBook(@PathVariable String id, Authentication authentication) {
        ModelAndView modelAndView = new ModelAndView("/web/viewer");
        try {
            BookDto bookDto = bookService.findByIdWithPDF(id, authentication.getName());
            modelAndView.addObject("bookDto", bookDto);
        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping("/view_register")
    public ModelAndView viewRegister() {
        ModelAndView modelAndView = new ModelAndView("/web/register");
        try {
            modelAndView.addObject("signUpObj", new ReaderDto());

        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return modelAndView;
    }

    @GetMapping(value = "/save_emotion")
    public ResponseEntity saveEmotion(Authentication authentication, @RequestParam("emotionList") String emotionList, @RequestParam("logId") String logId) {
        ReaderLog readerLog = null;
        try {
            readerLog = bookService.updateReaderLog(emotionList, logId);

        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return ResponseEntity.ok(readerLog);
    }

}
