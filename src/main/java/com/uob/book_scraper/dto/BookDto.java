package com.uob.book_scraper.dto;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Data
public class BookDto {
    private String id;
    private String bookName;
    private String author;
    private String publisher;
    private String language;
    private String category;
    private String publishedYear;
    private String isbnNo;
    private String description;
    private String bookStatus;
    private String scrapingId;
    private MultipartFile file;
    private MultipartFile thumbnail;
    private String bookType;
    private String updatedDateTime;


    private String fileString;
    private String thumbnailString;
    private String readerLogId;
    private List<FeedbackDto> feedbackDtoList;
}
