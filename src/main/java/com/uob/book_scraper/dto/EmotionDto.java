package com.uob.book_scraper.dto;

import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class EmotionDto {
    private List<String> emotionList;
    private Date closeDateTime;
    private String logId;
}
