package com.uob.book_scraper.dto;

import lombok.Data;

@Data
public class FeedbackDto {
    private String username;
    private String bookname;
    private String readDateTime;
    private String readEndDateTime;
    private String readedTime;
    private String emotion;
    private String feedback;
    private String verified;
    private String category;
}
