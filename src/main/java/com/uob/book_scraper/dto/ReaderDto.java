package com.uob.book_scraper.dto;

import lombok.Data;

@Data
public class ReaderDto {
    private int id;
    private String fullname;
    private String email;
    private String username;
    private String password;
    private String confirmPassword;
    private String userType;
    private String status;
}
