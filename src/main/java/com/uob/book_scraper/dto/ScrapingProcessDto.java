package com.uob.book_scraper.dto;

import lombok.Data;
import org.springframework.data.annotation.Id;

import java.util.Date;

@Data
public class ScrapingProcessDto {
    @Id
    private String id;
    private Date startDateTime;
    private Date endDateTime;
    private String status;
    private int scrapedBookCount;
}
