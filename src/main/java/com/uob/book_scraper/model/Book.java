package com.uob.book_scraper.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "books")
public class Book {
    @Id
    private String id;
    private String bookName;
    private String author;
    private String publisher;
    private String language;
    private String category;
    private String publishedYear;
    private String isbnNo;
    private String description;
    private String fileString;
    private String thumbnailString;
    private String bookStatus;
    private String scrapingId;
    private String bookType;
    private Date updatedDateTime;
}
