package com.uob.book_scraper.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document(collection = "reader")
public class Reader {
    @Id
    private int id;
    private String fullname;
    private String email;
    private String username;
    private String password;
    private String userType;
    private String status;
}
