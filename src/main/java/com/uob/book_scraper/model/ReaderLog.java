package com.uob.book_scraper.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.util.Date;

@Data
@Document(collection = "reader_log")
public class ReaderLog implements Serializable {
    @Id
    private String id;
    private String username;
    private String bookName;
    private String bookId;
    private Date readDateTime;
    private String category;
    private String emotion;
    private String feedback;
    private String verified;
    private Date closeDateTime;
}
