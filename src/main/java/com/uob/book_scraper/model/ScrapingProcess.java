package com.uob.book_scraper.model;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "scrapinglogs")
public class ScrapingProcess {
    @Id
    private String id;
    private Date startDateTime;
    private Date endDateTime;
    private String status;
    private int scrapedBookCount;
}
