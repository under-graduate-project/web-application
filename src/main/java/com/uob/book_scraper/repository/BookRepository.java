package com.uob.book_scraper.repository;

import com.uob.book_scraper.model.Book;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface BookRepository extends MongoRepository<Book, String> {

    @Query(value = "{}", sort = "{_id:-1}")
    public List<Book> findRBookByIdDesc();

    List<Book> findAllByBookStatus(String status);

    List<Book> findAllByBookStatusAndCategory(String status, String category);

    List<Book> findAllByBookStatusAndCategoryNotLike(String status, String category);


    @Query("{?0: {$regex: ?1,$options: 'i'}}")
    List<Book> findBookBySearchCriteria(String searchCriteria, String searchString);
}
