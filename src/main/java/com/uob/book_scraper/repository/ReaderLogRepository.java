package com.uob.book_scraper.repository;

import com.uob.book_scraper.model.ReaderLog;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface ReaderLogRepository extends MongoRepository<ReaderLog, String> {

    List<ReaderLog> findAllByUsernameOrderByReadDateTimeDesc(String username, Pageable pageable);

    List<ReaderLog> findAllByUsername(String username, Pageable pageable);

    List<ReaderLog> findAllByBookIdAndEmotionIsNotNullOrderByCloseDateTimeDesc(String bookId);
}
