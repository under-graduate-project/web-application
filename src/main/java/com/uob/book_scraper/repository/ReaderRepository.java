package com.uob.book_scraper.repository;

import com.uob.book_scraper.model.Reader;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;


public interface ReaderRepository extends MongoRepository<Reader, Integer> {

    @Query(value = "{}", sort = "{_id:-1}")
    public List<Reader> findReaderByIdDesc();

    public Reader findByUsernameAndStatus(String username, String status);
}
