package com.uob.book_scraper.repository;

import com.uob.book_scraper.model.ScrapingProcess;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ScrapingProcessRepository extends MongoRepository<ScrapingProcess, Integer> {
}
