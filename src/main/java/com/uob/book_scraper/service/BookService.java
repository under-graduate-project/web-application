package com.uob.book_scraper.service;

import com.uob.book_scraper.dto.BookDto;
import com.uob.book_scraper.dto.FeedbackDto;
import com.uob.book_scraper.model.ReaderLog;

import java.util.List;

public interface BookService {

    boolean saveBook(BookDto bookDto) throws Exception;

    List<BookDto> viewBooks() throws Exception;

    BookDto findById(String id) throws Exception;

    BookDto findByIdWithPDF(String id, String username) throws Exception;

    boolean updateBook(BookDto bookDto) throws Exception;

    List<BookDto> findActiveBooks(String active, String username) throws Exception;

    List<BookDto> findBookBySearchCriteria(String searchCriteria, String searchString) throws Exception;

    ReaderLog updateReaderLog(String emotionList, String logId) throws Exception;

    boolean verifyFeedback(String id) throws Exception;

    List<FeedbackDto> viewUserFeedbacks() throws Exception;
}
