package com.uob.book_scraper.service;

import com.uob.book_scraper.dto.ReaderDto;

import java.util.List;

public interface ReaderService {

    boolean registerReader(ReaderDto readerDto) throws Exception;

    List<ReaderDto> viewReaders() throws Exception;

    boolean blockReader(int id) throws Exception;
}
