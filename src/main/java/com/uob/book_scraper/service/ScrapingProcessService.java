package com.uob.book_scraper.service;

import com.uob.book_scraper.dto.ScrapingProcessDto;

import java.util.List;

public interface ScrapingProcessService {
    List<ScrapingProcessDto> viewScrapingProcesses() throws Exception;

    boolean checkScrapingStatus();
}
