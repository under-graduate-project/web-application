package com.uob.book_scraper.service.impl;

import com.convertapi.client.Config;
import com.convertapi.client.ConvertApi;
import com.convertapi.client.Param;
import com.uob.book_scraper.dto.BookDto;
import com.uob.book_scraper.dto.FeedbackDto;
import com.uob.book_scraper.model.Book;
import com.uob.book_scraper.model.ReaderLog;
import com.uob.book_scraper.repository.BookRepository;
import com.uob.book_scraper.repository.ReaderLogRepository;
import com.uob.book_scraper.service.BookService;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.InputStream;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class BookServiceImpl implements BookService {

    private static final String PDF_FILE = "data:application/pdf;base64,";
    private static final String EPUB_FILE = "data:application/epub+zip;base64,";

    private Logger logger = LoggerFactory.getLogger(BookServiceImpl.class);

    @Autowired
    private BookRepository bookRepository;

    @Autowired
    private ReaderLogRepository readerLogRepository;

    @Value("${app.convert-api-key}")
    private String secrete;

    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    @Override
    public boolean saveBook(BookDto bookDto) throws Exception {
        try {

            if (null != bookDto) {
                Book book = new Book();

                BeanUtils.copyProperties(bookDto, book);

                String bookString = convertBookToString(bookDto.getFile(), PDF_FILE);
                String thumbnailString = convertBookToString(bookDto.getThumbnail(), "data:" + bookDto.getThumbnail().getContentType() + ";base64,");

                book.setFileString(bookString);
                book.setThumbnailString(thumbnailString);
                book.setBookType("MANUAL");
                book.setUpdatedDateTime(new Date());

                bookRepository.save(book);
                return true;
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<BookDto> viewBooks() throws Exception {
        List<BookDto> bookDtos = new ArrayList<>();
        try {

            List<Book> all = bookRepository.findAll();

            for (Book book : all) {
                BookDto bookDto = new BookDto();

                BeanUtils.copyProperties(book, bookDto);
                if (null != book.getDescription() && !book.getDescription().isEmpty()) {
                    bookDto.setDescription(book.getDescription().substring(0, 15) + "...");
                } else {
                    bookDto.setDescription("N/A");
                }


                bookDtos.add(bookDto);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return bookDtos;
    }

    @Override
    public BookDto findById(String id) throws Exception {
        BookDto bookDto = new BookDto();

        List<FeedbackDto> feedbackDtoList = new ArrayList<>();
        try {

            Book book = bookRepository.findById(id).orElse(null);
            if (null != book) {
                String format = simpleDateFormat.format(book.getUpdatedDateTime());
                BeanUtils.copyProperties(book, bookDto);
                bookDto.setUpdatedDateTime(format);

                List<ReaderLog> readerLogs = readerLogRepository.findAllByBookIdAndEmotionIsNotNullOrderByCloseDateTimeDesc(book.getId());

                for (ReaderLog readerLog : readerLogs) {
                    FeedbackDto feedbackDto = new FeedbackDto();

                    feedbackDto.setEmotion(readerLog.getEmotion());
                    feedbackDto.setFeedback(readerLog.getFeedback());
                    feedbackDto.setUsername(readerLog.getUsername());
                    feedbackDto.setReadDateTime(simpleDateFormat.format(readerLog.getReadDateTime()));
                    feedbackDto.setVerified(readerLog.getVerified());

                    String timeDuration = getTimeDuration(readerLog.getCloseDateTime(), readerLog.getReadDateTime());
                    feedbackDto.setReadedTime(timeDuration);

                    feedbackDtoList.add(feedbackDto);
                }
                bookDto.setFeedbackDtoList(feedbackDtoList);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return bookDto;
    }

    private String getProcessedTime(long differentTime) {
        String time;
        if (differentTime < 9) {
            time = "0" + differentTime;
        } else {
            time = "" + differentTime;
        }
        return time;
    }

    @Override
    public BookDto findByIdWithPDF(String id, String username) throws Exception {
        BookDto bookDto = new BookDto();
        Config.setDefaultSecret(secrete);

        try {

            Book book = bookRepository.findById(id).orElse(null);
            if (null != book) {

                BeanUtils.copyProperties(book, bookDto);


                if (book.getBookType().equalsIgnoreCase("MANUAL")) {
                    bookDto.setFileString(book.getFileString());
                } else {
                    List<String> integer = ConvertApi.convert("epub", "pdf",
                            new Param("file", book.getFileString())
                    ).get().urls();

                    URL url = new URL(integer.get(0));
                    InputStream inputStream = url.openStream();
                    byte[] bytes = IOUtils.toByteArray(inputStream);

                    String encodeToString = Base64.getEncoder().encodeToString(bytes);

                    bookDto.setFileString(encodeToString);
                }
                ReaderLog readerLog = saveReadLog(book, username);
                bookDto.setReaderLogId(readerLog.getId());

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
        return bookDto;
    }

    @Override
    public boolean updateBook(BookDto bookDto) throws Exception {
        try {
            Book book = bookRepository.findById(bookDto.getId()).orElse(null);
            if (null != book) {
                String fileString = book.getFileString();
                String thumbnailString = book.getThumbnailString();
                String scrapingId = book.getScrapingId();

                BeanUtils.copyProperties(bookDto, book);
                book.setScrapingId(scrapingId);

                if (!bookDto.getFile().isEmpty()) {
                    String bookString = convertBookToString(bookDto.getFile(), EPUB_FILE);
                    book.setFileString(bookString);
                } else {
                    book.setFileString(fileString);
                }

                if (!bookDto.getThumbnail().isEmpty()) {
                    String thumbnail = convertBookToString(bookDto.getThumbnail(), "data:" + bookDto.getThumbnail().getContentType() + ";base64,");
                    book.setThumbnailString(thumbnail);
                } else {
                    book.setThumbnailString(thumbnailString);
                }

                bookRepository.save(book);
                return true;

            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<BookDto> findActiveBooks(String status, String username) throws Exception {
        List<BookDto> bookDtoList = new ArrayList<>();
        try {
            PageRequest pageRequest = PageRequest.of(0, 5);
            List<ReaderLog> allByUsername = readerLogRepository.findAllByUsernameOrderByReadDateTimeDesc(username, pageRequest);

            if (allByUsername.isEmpty()) {
                List<Book> bookList = bookRepository.findAllByBookStatus(status);

                for (Book book : bookList) {
                    BookDto bookDto = new BookDto();

                    BeanUtils.copyProperties(book, bookDto);

                    bookDtoList.add(bookDto);
                }
            } else {
                int max = 0;
                String category = "";
                Map<String, List<ReaderLog>> collect = allByUsername.stream().collect(Collectors.groupingBy(ReaderLog::getCategory));

                for (Map.Entry<String, List<ReaderLog>> stringListEntry : collect.entrySet()) {
                    if (max < stringListEntry.getValue().size()) {
                        max = stringListEntry.getValue().size();
                        category = stringListEntry.getKey();
                    }
                }
                String capitalize = StringUtils.capitalize(category);
                List<Book> booksByCat = bookRepository.findAllByBookStatusAndCategory(status, capitalize);
                List<Book> booksByNotCat = bookRepository.findAllByBookStatusAndCategoryNotLike(status, capitalize);

                for (Book book : booksByCat) {
                    BookDto bookDto = new BookDto();

                    BeanUtils.copyProperties(book, bookDto);

                    bookDtoList.add(bookDto);
                }

                for (Book book : booksByNotCat) {
                    BookDto bookDto = new BookDto();

                    BeanUtils.copyProperties(book, bookDto);

                    bookDtoList.add(bookDto);
                }


            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return bookDtoList;
    }

    @Override
    public List<BookDto> findBookBySearchCriteria(String searchCriteria, String searchString) throws Exception {
        List<BookDto> bookDtoList = new ArrayList<>();
        try {
            List<Book> bookList = bookRepository.findBookBySearchCriteria(searchCriteria, searchString);

            for (Book book : bookList) {
                BookDto bookDto = new BookDto();

                BeanUtils.copyProperties(book, bookDto);

                bookDtoList.add(bookDto);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return bookDtoList;
    }

    @Override
    public ReaderLog updateReaderLog(String emotionList, String logId) throws Exception {
        String replace = emotionList.replace("[", "").replace("]", "");
        String[] split = replace.split("[,]");

        try {
            List<String> strings = Arrays.asList(split);

            Map<String, List<String>> hashMap = new HashMap<>();
            strings.forEach(e -> {
                List<String> innerList = new ArrayList<>();
                if (hashMap.containsKey(e)) {
                    innerList = hashMap.get(e);
                }
                innerList.add(e);
                hashMap.put(e, innerList);
            });
            List<List<String>> valueList = hashMap.values().stream().collect(Collectors.toList());

            int max = 0;
            String emotion = "";
            for (List<String> stringList : valueList) {
                if (max < stringList.size()) {
                    max = stringList.size();
                    emotion = stringList.get(0);
                }
            }

            ReaderLog readerLog = readerLogRepository.findById(logId).orElse(null);

            if (null != readerLog) {
                readerLog.setEmotion(emotion.replace('"', ' ').trim());
                readerLog.setCloseDateTime(new Date());

                String feedback = getFeedbackByEmotion(emotion.replace('"', ' ').trim());

                readerLog.setFeedback(feedback);
                readerLog.setVerified("Unverified");

                return readerLogRepository.save(readerLog);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return null;
        }
        return null;
    }

    @Override
    public boolean verifyFeedback(String id) throws Exception {
        try {
            ReaderLog readerLog = readerLogRepository.findById(id).orElse(null);

            if (null != readerLog) {
                readerLog.setVerified("Verified");
                readerLogRepository.save(readerLog);
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return false;
    }

    @Override
    public List<FeedbackDto> viewUserFeedbacks() throws Exception {
        List<FeedbackDto> feedbackDtos = new ArrayList<>();
        try {
            List<ReaderLog> readerLogs = readerLogRepository.findAll();

            for (ReaderLog readerLog : readerLogs) {
                FeedbackDto feedbackDto = new FeedbackDto();

                feedbackDto.setEmotion(readerLog.getEmotion());
                feedbackDto.setFeedback(readerLog.getFeedback());
                feedbackDto.setUsername(readerLog.getUsername());
                feedbackDto.setBookname(readerLog.getBookName());
                feedbackDto.setCategory(readerLog.getCategory());
                feedbackDto.setReadDateTime(simpleDateFormat.format(readerLog.getReadDateTime()));
                feedbackDto.setReadEndDateTime(simpleDateFormat.format(readerLog.getCloseDateTime()));
                feedbackDto.setVerified(readerLog.getVerified());

                String timeDuration = getTimeDuration(readerLog.getCloseDateTime(), readerLog.getReadDateTime());
                feedbackDto.setReadedTime(timeDuration);

                feedbackDtos.add(feedbackDto);
            }

        } catch (Exception e) {
            logger.error(e.getMessage());
            throw e;
        }
        return feedbackDtos;
    }

    private String getTimeDuration(Date closedDateTime, Date readDateTime) {
        long time = closedDateTime.getTime() - readDateTime.getTime();

        long differentSec = time / 100 % 60;
        long differentMinutes = time / (60 * 1000) % 60;
        long differentHours = time / (60 * 60 * 1000);


        String hrs = getProcessedTime(differentHours);
        String mins = getProcessedTime(differentMinutes);
        String secs = getProcessedTime(differentSec);

        return hrs + ":" + mins + ":" + secs;
    }

    private String getFeedbackByEmotion(String emotion) {

        switch (emotion.toLowerCase()) {
            case "neutral":
                return "Good content Good content but it didn't change my emotions. It didn't hit my sweet spot.";
            case "sadness":
                return "Wow, what a great book. it made me cry. The author had managed to change my emotions.";
            case "happiness":
                return "I'm very impressed and I'm very happy with the content.";
            case "anger":
                return "Not happy with the content it makes me angry.";
            case "surprise":
                return "Omg, I'm very impressed. What a conceptual writing.";
            case "fear":
                return "Omg, I was scared with the content, by the way it was a very interesting book. author know how to express emotions.";
            case "disgust":
                return "I was disgust with the content, by the way it was a very interesting book. but it was hard to read.";
            default:
                return "N/A";
        }

    }

    private String convertBookToString(MultipartFile file, String type) throws Exception {

        String encodeToString = Base64.getEncoder().encodeToString(file.getBytes());
        if (!type.contains("pdf")) {
            return type.concat(encodeToString);
        }
        return encodeToString;

    }


    private ReaderLog saveReadLog(Book book, String username) {
        ReaderLog savedLog = new ReaderLog();
        try {
            ReaderLog readerLog = new ReaderLog();
            readerLog.setUsername(username);
            readerLog.setBookName(book.getBookName());
            readerLog.setBookId(book.getId());
            readerLog.setCategory(book.getCategory());
            readerLog.setReadDateTime(new Date());
            readerLog.setEmotion(null);

            savedLog = readerLogRepository.save(readerLog);
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return savedLog;
    }
}
