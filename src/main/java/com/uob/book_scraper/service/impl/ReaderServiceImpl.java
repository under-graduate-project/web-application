package com.uob.book_scraper.service.impl;

import com.uob.book_scraper.dto.ReaderDto;
import com.uob.book_scraper.model.Reader;
import com.uob.book_scraper.repository.ReaderRepository;
import com.uob.book_scraper.service.ReaderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ReaderServiceImpl implements ReaderService {


    @Autowired
    private ReaderRepository readerRepository;

    @Autowired
    private BCryptPasswordEncoder encoder;

    private Logger logger = LoggerFactory.getLogger(ReaderServiceImpl.class);

    @Override
    public boolean registerReader(ReaderDto readerDto) throws Exception {
        try {

            if (null != readerDto) {
                Reader reader = new Reader();

                BeanUtils.copyProperties(readerDto, reader);

                String encode = encoder.encode(readerDto.getPassword());
                reader.setPassword(encode);

                int newId = getReaderNewId();

                reader.setId(newId);
                reader.setUserType("READER");
                reader.setStatus("Active");
                readerRepository.save(reader);
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return false;
    }

    @Override
    public List<ReaderDto> viewReaders() throws Exception {
        List<ReaderDto> readerDtoList = new ArrayList<>();
        try {

            List<Reader> all = readerRepository.findAll();

            for (Reader reader : all) {
                ReaderDto readerDto = new ReaderDto();

                BeanUtils.copyProperties(reader, readerDto);

                readerDtoList.add(readerDto);
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
        }
        return readerDtoList;
    }

    @Override
    public boolean blockReader(int id) throws Exception {
        try {
            Reader reader = readerRepository.findById(id).orElse(null);
            if (null != reader) {
                if (reader.getStatus().equalsIgnoreCase("Active")) {
                    reader.setStatus("Inactive");
                }else {
                    reader.setStatus("Active");
                }
                readerRepository.save(reader);
                return true;
            }
        } catch (Exception e) {
            logger.error(e.getMessage());
            return false;
        }
        return false;
    }

    private int getReaderNewId() {
        Reader readerByIdDesc = readerRepository.findReaderByIdDesc().stream().findFirst().orElse(null);

        if (null != readerByIdDesc) {
            return readerByIdDesc.getId() + 1;
        }
        return 0;
    }
}
