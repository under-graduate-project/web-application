package com.uob.book_scraper.service.impl;

import com.uob.book_scraper.dto.ScrapingProcessDto;
import com.uob.book_scraper.model.ScrapingProcess;
import com.uob.book_scraper.repository.ScrapingProcessRepository;
import com.uob.book_scraper.service.ScrapingProcessService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ScrapingProcessServiceImpl implements ScrapingProcessService {

    @Autowired
    private ScrapingProcessRepository scrapingProcessRepository;

    private Logger logger = LoggerFactory.getLogger(ScrapingProcessServiceImpl.class);

    @Override
    public List<ScrapingProcessDto> viewScrapingProcesses() throws Exception {
        List<ScrapingProcessDto> scrapingProcessDtoList = new ArrayList<>();
        try {
            List<ScrapingProcess> all = scrapingProcessRepository.findAll();

            for (ScrapingProcess scrapingProcess : all) {
                ScrapingProcessDto scrapingProcessDto = new ScrapingProcessDto();
                BeanUtils.copyProperties(scrapingProcess,scrapingProcessDto);


                scrapingProcessDtoList.add(scrapingProcessDto);
            }
        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return scrapingProcessDtoList;
    }

    @Override
    public boolean checkScrapingStatus() {
        try {
            List<ScrapingProcess> all = scrapingProcessRepository.findAll();

            for (ScrapingProcess scrapingProcess : all) {
                if (scrapingProcess.getStatus().equalsIgnoreCase("INPROGRESS")){
                    return false;
                }
            }
            return true;
        } catch (Exception e) {
             logger.error(e.getMessage());
        }
        return true;
    }
}
