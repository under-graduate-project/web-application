package com.uob.book_scraper.service.impl;

import com.uob.book_scraper.model.Reader;
import com.uob.book_scraper.repository.ReaderRepository;
import com.uob.book_scraper.service.UserDetailService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashSet;
import java.util.Set;

@Service
@Transactional
public class UserDetailServiceImpl implements UserDetailService {

    @Autowired
    private ReaderRepository readerRepository;

    private Logger logger = LoggerFactory.getLogger(UserDetailServiceImpl.class);

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            Reader byUsername = readerRepository.findByUsernameAndStatus(username,"Active");

            if (null != byUsername) {

                Set<GrantedAuthority> grantedAuthorities = buildUserAuthority(byUsername);
                return new User(byUsername.getUsername(), byUsername.getPassword(), grantedAuthorities);
            } else {
                throw new UsernameNotFoundException("Username not found");
            }
        } catch (Exception e) {
             logger.error(e.getMessage());
            throw new UsernameNotFoundException(e.getMessage());
        }
    }


    private Set<GrantedAuthority> buildUserAuthority(Reader reader) {
        Set<GrantedAuthority> grantedAuthorities = new HashSet<>();
        grantedAuthorities.add(new SimpleGrantedAuthority("ROLE_"+reader.getUserType()));
        return grantedAuthorities;
    }
}
