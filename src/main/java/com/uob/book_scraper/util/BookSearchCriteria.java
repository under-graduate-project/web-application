package com.uob.book_scraper.util;

public enum BookSearchCriteria {
    AUTHOR("author"),
    BOOK_NAME("bookName"),
    CATEGORY("category"),
//    ISBN_NO("isbnNo"),
//    PUBLISHER("publisher"),
//    PUBLISHED_YEAR("publishedYear"),
    LANGUAGE("language");

    public final String searchCriteria;

    BookSearchCriteria(String searchCriteria) {
        this.searchCriteria = searchCriteria;
    }

}
