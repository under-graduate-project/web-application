package com.uob.book_scraper.util;

public enum ScrapingCriteria {
    THRILLER("Thriller"),
    ROMANTIC("romance"),
    ADVENTURE("Adventure"),
    HORROR("Horror"),
    BIOGRAPHY("biography"),
    COMEDY("comedy");


    public final String scrapingCriteria;

    ScrapingCriteria(String scrapingCriteria) {
        this.scrapingCriteria = scrapingCriteria;
    }
}
